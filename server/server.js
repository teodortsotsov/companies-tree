var serverConstants = require('./serverConstants');
var jsonServer = require('json-server');
var server = jsonServer.create();
var router = jsonServer.router(require('./db.js')());
var middlewares = jsonServer.defaults();

server.use(middlewares);
server.use(router);
server.listen(serverConstants.port, function () {
    console.log('JSON Server is running on port ' + serverConstants.port);
});
