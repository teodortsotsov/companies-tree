var companies = require('../data/companies.json');
var company_addresses = require('../data/company-addresses.json');
var employees = require('../data/employees.json');
var projects = require('../data/projects.json');
// and so on

module.exports = function () {
    return {
        companies,
        company_addresses,
        employees,
        projects,
    };
};
