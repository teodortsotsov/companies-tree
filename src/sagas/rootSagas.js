import { all } from 'redux-saga/effects';
import companiesSagas from '../components/Companies/CompaniesSagas';

export default function* rootSagas() {
    yield all([...companiesSagas]);
}
