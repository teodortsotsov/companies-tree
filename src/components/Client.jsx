import React, { Component } from 'react';
import { Provider } from 'react-redux';
import store from '../store/configureStore';
import Companies from './Companies/Companies';

class Client extends Component {
    render() {
        return (
            <Provider store={store}>
                <div className="client">
                    <Companies />
                </div>
            </Provider>
        );
    }
}
export default Client;
