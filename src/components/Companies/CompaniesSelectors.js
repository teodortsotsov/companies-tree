import { createSelector } from 'reselect';

const getCompanies = (state) => state.companiesReducer;

export const companiesSelector = createSelector(
    getCompanies,
    (companies) => companies
);
