import { COMPANIES_LIST_REQUESTING } from './CompaniesConstants';

export const getCompaniesList = () => ({ type: COMPANIES_LIST_REQUESTING });
