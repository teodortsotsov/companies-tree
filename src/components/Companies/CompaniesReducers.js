import {
    COMPANIES_LIST_REQUESTING,
    COMPANIES_LIST_SUCCESS,
    COMPANIES_LIST_ERROR,
} from './CompaniesConstants';

export const companiesReducer = (state = { records: [] }, action) => {
    switch (action.type) {
        case COMPANIES_LIST_REQUESTING:
            return {
                ...state,
            };
        case COMPANIES_LIST_SUCCESS:
            return {
                ...state,
                records: action.records,
            };
        case COMPANIES_LIST_ERROR:
            return {
                ...state,
                error: action.error,
            };
        default:
            return state;
    }
};
