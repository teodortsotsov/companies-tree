import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getCompaniesList } from './CompaniesActions';
import Company from './Company';
import { companiesSelector } from './CompaniesSelectors';
import './Companies.scss';

class Companies extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedCompany: { id: -1, name: '' },
        };
    }
    componentDidMount() {
        this.props.getCompaniesList();
    }
    selectCompany = (company) => {
        this.setState({ selectedCompany: company });
    };
    render() {
        return (
            <ul id="companies-list">
                {this.props.companies.records.map((company) => {
                    return (
                        <Company
                            key={company.id}
                            selected={
                                company.id === this.state.selectedCompany.id
                            }
                            company={company}
                            selectCompany={this.selectCompany}
                        />
                    );
                })}
            </ul>
        );
    }
}
const mapStateToProps = (state) => ({
    companies: companiesSelector(state),
});

const mapDispatchToProps = (dispatch) => {
    return {
        getCompaniesList: () => dispatch(getCompaniesList()),
    };
};

Companies.propTypes = {
    getCompaniesList: PropTypes.func,
    companies: PropTypes.arrayOf(PropTypes.objectOf({})),
};

export default connect(mapStateToProps, mapDispatchToProps)(Companies);
