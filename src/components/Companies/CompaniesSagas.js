import {
    COMPANIES_LIST_REQUESTING,
    COMPANIES_LIST_SUCCESS,
    COMPANIES_LIST_ERROR,
} from './CompaniesConstants';
import CompanyService from '../../services/companyServices';
import { call, takeLatest, put } from 'redux-saga/effects';
function* getCompaniesList() {
    try {
        const companiesList = yield call(CompanyService.getCompaniesList);
        console.log('companiesList');
        console.log(companiesList);
        if (companiesList.length > 0) {
            yield put({
                type: COMPANIES_LIST_SUCCESS,
                records: companiesList,
            });
        } else {
            yield put({
                type: COMPANIES_LIST_ERROR,
            });
        }
    } catch (error) {
        yield put({ type: COMPANIES_LIST_ERROR });
    }
}

function* getCompaniesListWatcher() {
    yield takeLatest(COMPANIES_LIST_REQUESTING, getCompaniesList);
}

const companiesSagas = [getCompaniesListWatcher()];

export default companiesSagas;
