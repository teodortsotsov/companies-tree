import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Company extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        const { company } = this.props;
        const { name, id } = company;
        return (
            <li
                className={this.props.selected ? 'active' : ''}
                onClick={() => {
                    this.props.selectCompany(company);
                }}
            >
                {name}
            </li>
        );
    }
}

Company.propTypes = {
    company: PropTypes.objectOf(
        PropTypes.shape({
            id: PropTypes.string,
            name: PropTypes.string,
        })
    ),
    selectCompany: PropTypes.func,
    selected: PropTypes.bool,
};

export default Company;
