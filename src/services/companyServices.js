import axios from 'axios';
import { host, port } from '../../server/serverConstants';
const url = host + ':' + port;
const CompanyServices = {
    getCompaniesList: function () {
        let companiesList = [];
        return axios
            .get(url + '/companies')
            .then(function (response) {
                if (response.data) {
                    companiesList = response.data;
                }
                return companiesList;
            })
            .catch(function (error) {
                console.log('Error getting companies list' + error);
                return companiesList;
            });
    },
};

export default CompanyServices;
