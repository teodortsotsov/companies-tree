import { combineReducers } from 'redux';
import { companiesReducer } from '../components/Companies/CompaniesReducers';

const rootReducer = combineReducers({ companiesReducer });

export default rootReducer;
